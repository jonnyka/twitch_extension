console.log('Twitch feed extension loaded!');

$(document).ready(function () {
    function processData(text) {
        var textLines = text.split(/\r\n|\n/),
            twitchers = [],
            i = 2;
    
        for (i; i < textLines.length; i++) {
            var data = textLines[i].split(',');

            twitchers.push(data[1]);
        }

        return twitchers;
    }

    function readSheets() {
        //var url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQ1N0Txl1p51c5ki-MQ7Z0h8wT4o094d18ZZU01OyMDvKIj7beRSPM7MI-YwHUbrg/pub?gid=1127963973&single=true&output=csv";
        var url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vReYbb8xuIiTZ9GVOsXtyb0Jjjw5SjZm_yxW6Tr5RJWxUAlaj_-FrFd48W72YfWPIlPTJ9DzwLl0xpj/pub?gid=1127963973&single=true&output=csv";

        $.ajax({
            type: "GET",
            url: url,
            dataType: "text",
            success: function(data) {
                var twitchers = processData(data),
                    names = $('.preview-card-titles__subtitle-wrapper'),
                    name = '';

                console.log(twitchers);
                names.each(function() {
                    name = $(this).text();

                    if (twitchers.indexOf(name) > -1) {
                        $(this).addClass('yt-red');
                        $(this).find('a').addClass('yt-red');
                    } else {
                        $(this).addClass('yt-green');
                        $(this).find('a').addClass('yt-green');
                    }
                });
            }
         });
    }

    setTimeout(readSheets, 500);
});
